var calculator = angular.module("calculatorApp", []);
calculator.controller("CalculatorCtrl", CalculatorCtrl);

function CalculatorCtrl() {
    this.resultValue = 0;
    this.buttonClicked = function (button) {
        this.selectedOperation = button;
    }
    this.computeResult = function () {
        var input1 = parseFloat(this.input1);
        var input2 = parseFloat(this.input2);

        if (this.selectedOperation == '+') {
            this.resultValue = input1 + input2;
        }
        else if (this.selectedOperation == '-') {
            this.resultValue = input1 - input2;
        }
        else if (this.selectedOperation == '*') {
            this.resultValue = input1 * input2;
        }
        else if (this.selectedOperation == '/') {
            this.resultValue = input1 / input2;
        }


    }
}